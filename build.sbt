enablePlugins(JavaAppPackaging)

name := "im-server"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val akkaVersion = "2.4.12"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion,
    "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
    "com.github.dnvriend" %% "akka-persistence-inmemory" % "1.2.12",
    "org.scalatest" %% "scalatest" % "2.2.6" % "test",
    "io.undertow" % "undertow-core" % "1.2.11.Final",
    "io.undertow" % "undertow-servlet" % "1.2.11.Final",
    "io.undertow" % "undertow-websockets-jsr" % "1.2.11.Final",
    "org.json4s" %% "json4s-jackson" % "3.3.0"
  )
}

mainClass in Compile := Some("top.im.Boot")

mappings in Universal += {
  // we are using the reference.conf as default reference.conf
  // the user can override settings here
  val conf = (resourceDirectory in Compile).value / "reference.conf"
  conf -> "conf/application.conf"
}
