package top.im

import akka.actor.{Actor, ActorLogging}
import top.im.messages.PushMessage
import io.undertow.websockets.core.{BufferedTextMessage, WebSocketChannel, WebSockets}

/**
 * Created by jf on 16/1/24.
 */
class WebSocketServer(channel: WebSocketChannel) extends Actor with ActorLogging {
  override def receive: Receive = {
    case m: BufferedTextMessage =>
      val message = m.getData
      log.info("receive : {}", message)
      send(message)
    case p: PushMessage => send(p.message)
  }

  def send(msg: String) = {
    WebSockets.sendText(msg, channel, null)
  }
}
