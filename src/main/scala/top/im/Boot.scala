package top.im

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.util.Timeout
import com.typesafe.config.ConfigFactory

/**
  * Created by jf on 2016/11/14.
  */
object Boot extends App {

  val defaults = ConfigFactory.load()
  val port = if (args != null && args.length > 0) args.apply(0) else defaults.getInt("akka.remote.netty.tcp.port")

  val config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port).
    withFallback(defaults)

  val wsPort = config.getInt("undertow.port")
  val wsHost = config.getString("undertow.host")

  implicit val actorSystem = ActorSystem("push-server", config)


  implicit val pushServer = PushServer(s"${wsHost}:${wsPort}/ws")

  HttpServer.start(wsHost, wsPort)

}
