package top.im.messages

import io.undertow.websockets.core.WebSocketChannel
import io.undertow.websockets.spi.WebSocketHttpExchange

/**
  * Created by jf on 2016/11/14.
  */
case class NewConnect(id: String, exchange: WebSocketHttpExchange, channel: WebSocketChannel)
