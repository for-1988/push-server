package top.im.messages

/**
 * Created by jf on 16/1/24.
 */

sealed trait Event

case class PushChildren(message: String)

case class Push(id: String, message: String) extends Event

case class Pushed(id: String) extends Event

case class PushMessage(message: String)

case class Letter(from: String, to: String, message: String)
