#push-server

## 原理
* 使用undertow处理websocket连接以及暴露/push rest接口，接受外部系统传入需要推送的消息
* 使用akka的分片集群来做消息分片，与客户端连接时分配策略保持一致。可以换成更优的分片方式，如一致性hash

